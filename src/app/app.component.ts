import { Component } from '@angular/core';
import { of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  inputValue: string = '';
  result: string = '';

  calculate(): void {
    of(this.parseExpression(this.inputValue)).pipe(
      map(operation => this.performOperation(operation)),
      map(result => result.toString()),
      catchError(err => of('Ошибка: ' + err.message))
    ).subscribe(result => this.result = result);
  }

  private parseExpression(expression: string): Array<number | string> {
    const tokens = expression.match(/[+\-*/]|\d+(?:\.\d+)?/g);
    if (!tokens || tokens.length > 3) {
      throw new Error('Неверное выражение');
    }
    return tokens.map(token => isNaN(Number(token)) ? token : Number(token));
  }

  private performOperation(operation: any[]): number {
    switch(operation[1]){
      case '+':
        return operation[0] + operation[2];
      case '-':
        return operation[0] - operation[2];
      case '*':
        return operation[0] * operation[2];
      case '/':
        return operation[0] / operation[2];
    }

    throw new Error('Неверная операция');
  }
}
